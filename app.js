const Discord = require('discord.js');
const Keyv = require('keyv');

const config = require('./config.json');

const client = new Discord.Client();
console.log('process.env.DATABASE_URL:', process.env.DATABASE_URL);
const keyv = new Keyv(process.env.DATABASE_URL);
keyv.on('error', err => console.log('Keyv error:', err));
const plugins = {};

function loadPlugin(pluginName) {
    console.log("Loading " + pluginName + " plugin...");
    plugins[pluginName] = require('./plugins/' + pluginName + '/plugin.js');
    return plugins[pluginName].init({ config, client, keyv });
}

async function shutdown() {
    console.log('Shutting down...');
    try {
        for (const plugin in plugins) {
            if (plugins[plugin].onShutdown) {
                await plugins[plugin].onShutdown();
            }
        }
    } catch (e) {
        console.log('Error during shutdown process:', e);
    }
    process.exit(0);
}

process.on('SIGTERM', function () {
    console.log('SIGTERM fired');
    shutdown();
})

async function main() {
    try {
        await client.login(process.env.DISCORD_BOT_TOKEN);
        for (pluginName of config.plugins) {
            await loadPlugin(pluginName);
        }
    } catch (e) {
        console.error("ERROR:", e);
        process.exit();
    }
}

main();

