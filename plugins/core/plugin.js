function init(app) {
    app.client.once('ready', () => {
        console.log('Ready!');
    });

    app.client.once('reconnecting', () => {
        console.log('Reconnecting!');
    });

    app.client.once('disconnect', () => {
        console.log('Disconnect!');
    });

    app.client.on("message", async message => {
        if (message.author.bot) return;
        if (!message.content.startsWith(app.config.prefix)) return;
        console.log("Incoming command:", message.content);
    });

    console.log('core plugin loaded');
}

module.exports = {
    init
};
