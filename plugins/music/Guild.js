const ytdl = require("ytdl-core");
const ytpl = require('ytpl');

const songStruct = {
    title: null,
    url: null,
    temporary: false
};

class Guild {

    constructor(client, id, properties = {}) {
        Object.assign(this, {
            id,
            client,
            initialOffset: 0,
            intervalObject: null,
            connection: null,
            offset: 0,
            playing: false,
            queue: [],
            repeat: false,
            requeue: true,
            songIsFinished: true,
            volume: 100,
            ...properties
        });
    }

    clear() {
        this.queue = [];
        this.stop();
    }

    async fetchURL(url) {
        if (!url) {
            throw "no url was provided";
        }
        const matches = url.match(new RegExp('^https?://www.youtube.com/playlist\\?list=([a-zA-Z0-9-]+)'));
        if (matches) {
            const songs = await this.fetchYTPlaylist(matches[1]);
            return songs.map(v => ({ ...songStruct, title: v.title, url: v.url }));
        } else {
            const songInfo = await ytdl.getInfo(url);
            return [{ ...songStruct, title: songInfo.title, url: songInfo.video_url }];
        }
    }

    async fetchYTPlaylist(playlistId) {
        return (await ytpl(playlistId, { limit: 0 })).items.map(i => ({ ...songStruct, title: i.title, url: i.url_simple }));
    }

    async join(voiceChannel) {
        if (!voiceChannel) {
            throw "no channel specified"
        }
        const permissions = voiceChannel.permissionsFor(this.client.user);
        if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
            throw ("not enough permissions to join or speak in the voice channel");
        }
        this.connection = (await voiceChannel.join())
            .on("disconnect", () => { console.log("VoiceConnection disconnect event"); })
            .on("error", (error) => { console.log("VoiceConnection error event:", error); })
            .on("failed", (error) => { console.log("VoiceConnection failed event:", error); })
            .on("newSession", () => { console.log("VoiceConnection newSession event"); })
            .on("ready", () => { console.log("VoiceConnection ready event"); })
            .on("reconnecting", () => { console.log("VoiceConnection reconnecting event"); })
            .on("warn", (warning) => { console.log("VoiceConnection debug event:", warning); });
        return this;
    }

    async leave() {
        this.stop();
        if (this.connection) {
            await this.connection.disconnect();
            this.connection = null;
        }
        return this;
    }

    next() {
        if (this.requeue && !this.queue[0].temporary) {
            this.queue.push(this.queue[0]);
        }
        this.stop();
        this.queue.shift();
        this.play();
    }

    play() {
        this.playing = true;
        if (!this.intervalObject) {
            this.intervalObject = setInterval(() => { this.doPlaying(); }, 100)
        }
    }

    doPlaying() {
        if (!this.playing || !this.queue.length) return this.stop();
        if (this.songIsFinished || !this.connection.dispatcher) {
            this.songIsFinished = false;
            this.initialOffset = this.offset;
            const song = this.queue[0];
            const stream = ytdl(song.url, { filter: 'audioonly' });
            const dispatcher = this.connection
                .play(stream, { seek: this.offset < 300 ? this.offset : 0 /* do not seek more than 5min */ })
                .on("finish", () => {
                    this.songIsFinished = true;
                    this.offset = 0;
                    if (this.repeat) return; // in repeat mode, no queue update
                    if (this.requeue && !song.temporary) {
                        this.queue.push(song);
                    }
                    this.queue.shift();
                })
                .on("debug", (info) => { console.log("StreamDispatcher debug event:", info); })
                .on("error", (error) => { console.log("StreamDispatcher error event:", error); })
                .on("start", () => { console.log("StreamDispatcher start event"); })
                .on("volumeChange", (oldVolume, newVolume) => { console.log("StreamDispatcher volumeChange event:", oldVolume, newVolume); });
            dispatcher.setVolumeLogarithmic(this.volume / 100);
        }
        this.offset = this.initialOffset + ((this.connection && this.connection.dispatcher) ? this.connection.dispatcher.streamTime / 1000 : 0);
    }

    async enqueue(url, temporary = false) {
        const songs = (await this.fetchURL(url)).map((s) => {
            s.temporary = temporary; return s;
        });
        if (temporary) {
            // if there are already temporary songs in the queue, delete them
            this.queue = this.queue.filter(s => !s.temporary);
            this.queue.unshift(...songs);
            this.stop(); // interrupt current playing song
        } else {
            this.queue.push(...songs);
        }
        return songs;
    }

    stop() {
        if (this.connection && this.connection.dispatcher) {
            this.connection.dispatcher.destroy();
        }
        this.playing = false;
        this.songIsFinished = true;
        this.offset = 0;
        clearInterval(this.intervalObject);
        this.intervalObject = null;
    }

    toggleRepeat() {
        this.repeat = !this.repeat;
        return this.repeat;
    }

    toggleRequeue() {
        this.requeue = !this.requeue;
        return this.requeue;
    }

    vol(volume = null) {
        if (volume !== null) {
            this.volume = parseFloat(volume);
            if (this.volume < 0) this.volume = 0;
            if (this.volume > 100) this.volume = 100;
        }
        if (this.connection && this.connection.dispatcher) {
            this.connection.dispatcher.setVolumeLogarithmic(this.volume / 100);
        }
        return this.volume;
    }
}

module.exports = Guild;