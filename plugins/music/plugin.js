const fs = require('fs');
const Guild = require('./Guild');

let client = null;
let keyv = null;

const commands = [
    { fn: clear, aliases: ["clear"] },
    { fn: help, aliases: ["help"] },
    { fn: join, aliases: ["join"] },
    { fn: leave, aliases: ["leave"] },
    { fn: nowplaying, aliases: ["nowplaying", "np"] },
    { fn: play, aliases: ["play"] },
    { fn: queue, aliases: ["queue", "q"] },
    { fn: repeat, aliases: ["repeat", "rep"] },
    { fn: requeue, aliases: ["requeue", "req"] },
    { fn: save, aliases: ["save"] },
    { fn: skip, aliases: ["skip", "next"] },
    { fn: stop, aliases: ["stop"] },
    { fn: volume, aliases: ["volume", "vol"] }
];

const guilds = new Map();

async function _createGuild(guildId, config) {
    const guild = new Guild(client, guildId, { volume: config.defaultVolume });
    await guild.enqueue('https://www.youtube.com/playlist?list=PL6bv-nSJmmJ9ZVeQquV4kF7koSqWhvn7X');
    guilds.set(guildId, guild);
}

async function _loadState(key) {
    const state = await keyv.get(key);
    console.log("state:", state);
    if (!state) {
        console.log(`${key} not found. No state loaded.`);
        return;
    }
    console.log(`Saved state found. Loading ${key}...`);
    for (g of state.guilds) {
        const guild = new Guild(client, g.id, {
            playing: g.playing,
            queue: g.queue,
            requeue: g.requeue,
            songIsFinished: g.songIsFinished,
            volume: g.volume,
            offset: g.offset
        });
        guilds.set(guild.id, guild);
        if (g.channel) {
            await guild.join(await client.channels.fetch(g.channel));
            if (g.playing) {
                guild.play();
            }
        }
    }
}

function _saveState(key) {
    const state = {
        guilds: []
    };
    for (var [guildId, guild] of guilds) {
        state.guilds.push({
            id: guild.id,
            playing: guild.playing,
            queue: guild.queue,
            requeue: guild.requeue,
            songIsFinished: guild.songIsFinished,
            volume: guild.volume,
            channel: guild.connection ? guild.connection.channel.id : null,
            offset: guild.offset
        });
    }
    keyv.set(key, state);
}

function clear(guild, message) {
    guild.clear();
    message.channel.send(`Queue cleared.`);
}

function help(guild, message) {
    const cmdList = commands.reduce((acc, cur) => {
        const out = cur.aliases[0] + (cur.aliases.length > 1 ? ` (${cur.aliases.slice(1).join(', ')})` :'');
        acc.push(out);
        return acc;
    }, []);
    message.channel.send('Music command list (and aliases):```' + cmdList.join(', ') + '```');
}

async function join(guild, message) {
    const voiceChannel = message.member.voice.channel;
    if (!voiceChannel) {
        return message.channel.send("You need to be in a voice channel to play music!");
    }
    await guild.join(voiceChannel);
}

async function leave(guild) {
    await guild.leave();
}

function nowplaying(guild, message) {
    return message.channel.send(`Now playing: ${guild.queue[0].url}`);
}

async function play(guild, message) {
    const args = message.content.split(" ");
    if (args[1]) {
        await queue(guild, message, true);
    }
    if (!guild.queue.length) {
        return message.channel.send('Queue is empty. Nothing to play.');
    }
    try {
        if (!guild.connection) await join(guild, message);
    } catch (e) {
        guild.stop();
        throw e;
    }
    if (!guild.playing) {
        message.channel.send(`Start playing: **${guild.queue[0].title}**`);
    }
    guild.play();
}

async function queue(guild, message, temporary = false) {
    const args = message.content.split(" ");
    const songs = await guild.enqueue(args[1], temporary);
    if (songs.length > 1) {
        message.channel.send(`${songs.length} songs added to the queue`);
    } else if (songs.length == 1) {
        message.channel.send(`Added to the queue : \`${songs[0].title}\``);
    }
}

function repeat(guild, message) {
    const result = guild.toggleRepeat();
    message.channel.send("Repeat: " + (result ? "activated" : "deactivated"));
}

function requeue(guild, message) {
    const result = guild.toggleRequeue();
    message.channel.send("Requeue: " + (result ? "activated" : "deactivated"));
}

function skip(guild, message) {
    guild.next();
}

async function save(guild, message) {
    await _saveState('music-state');
}

function stop(guild) {
    guild.stop();
}

function volume(guild, message) {
    const args = message.content.split(" ");
    const volume = guild.vol(args[1]);
    message.channel.send(`Volume: ${volume} / 100`);
}

async function onShutdown() {
    console.log('Shutdown asked. Saving music plugin state...');
    await _saveState('music-state');
}

async function init(app) {
    const prefix = app.config.prefix;
    client = app.client;
    keyv = app.keyv;
    client.on("message", async message => {
        try {
            if (message.author.bot) return;
            if (!message.content.startsWith(prefix)) return;
            if (!guilds.has(message.guild.id)) {
                console.log(`Guild ${message.guild.id} does not exists. Creating it...`);
                await _createGuild(message.guild.id, app.config);
            }
            const guild = guilds.get(message.guild.id);
            for (let cmd of commands) {
                if (message.content.match(new RegExp(`^${prefix}(${cmd.aliases.join('|')})?( |$)`))) {
                    return await cmd.fn(guild, message);
                }
            }
        } catch (e) {
            console.error(e);
            return message.channel.send("Error: ```" + e + "```");
        }
    });
    await _loadState('music-state');
    setInterval(() => { _saveState('music-state'); }, 10000)
    console.log('music plugin loaded');
}

module.exports = {
    init,
    onShutdown
};
